
Drupal.behaviors.dock = function() {
  $('<div id="dockContainer"></div>').appendTo("body").load(Drupal.settings.basePath + "dock", function() {
    switch (Drupal.settings.dock.position) {
      case 'bottom':
        var labels_position = "tc";
        break;
      case 'top':
        var labels_position = "bc";
        break;
      case 'left':
        var labels_position = "mr";
        break;
      case 'right':
        var labels_position = "ml";
        break;
      default:
        var labels_position = false;
        break;
    }
    $('#dock').jqDock({
      align: Drupal.settings.dock.position,
      distance: 125,
      duration: 200,
      labels: Drupal.settings.dock.labels ? labels_position : false,
      loader: 'jquery',
      size: Drupal.settings.dock.default_size
    });
    $(document).bind('scroll', dockResetPosition);
    setTimeout(function() {
      dockResetPosition();
    }, 200);
  });
}

function dockResetPosition() {
  var width = 0, height = 0;
  var dock_size = $('#dock li').size() * $('img.dockItem').width();
  if (typeof(window.innerWidth) == 'number' ) {
    width = window.innerWidth;
    height = window.innerHeight;
  } //Non-IE
  else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
    width = document.documentElement.clientWidth;
    height = document.documentElement.clientHeight;
  } //IE 6+ in 'standards compliant mode'
  else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
    width = document.body.clientWidth;
    height = document.body.clientHeight;
  } //IE 4 compatible
  // @todo This is bad algorithm for centering dock.
  var top = height/2 - dock_size/2;
  var left = width/2 - dock_size/2;
  switch (Drupal.settings.dock.position) {
    case 'bottom':
      $('#dock').css('bottom', Drupal.settings.dock.default_size + "px");
      $('#dock').css('left', left + "px");
      break;
    case 'top':
      $('#dock').css('top', 0);
      $('#dock').css('left', left + "px");
      break;
    case 'left':
      $('#dock').css({
        'top': top + "px",
        'left': 0
      }).find('li').css('float', 'none');
      break;
    case 'right':
      $('#dock').css({
        'top': top + "px",
        'right': Drupal.settings.dock.default_size + "px"
      }).find('li').css('float', 'none');
      break;
  }
}