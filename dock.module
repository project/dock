<?php

/**
 * Implementation of hook_menu().
 */
function dock_menu() {
	$items = array();

	$items['admin/settings/dock'] = array(
    'access arguments' 	=> 	array('administer site configuration'),
		'description'				=>	t('Renders the site menus in form of a dock.'),
		'page callback'			=>	'drupal_get_form',
		'page arguments'		=>  array('dock_admin_settings'),
		'title'							=>	t('Dock'),
		'type'							=>	MENU_NORMAL_ITEM,
	);

	$items['dock'] = array(
	  'access arguments' 	=> 	array('access content'),
		'page callback'			=>	'dock_load_content',
		'type'							=>	MENU_CALLBACK,
	);

	return $items;
}

function dock_init() {
	drupal_add_js(array(
		'dock' => array(
			'position' => variable_get('dock_position', 'bottom'),
			'labels' => variable_get('dock_labels', FALSE),
			'default_size' => variable_get('dock_size', 48),
		),
	), 'setting');
	drupal_add_js(drupal_get_path('module', 'dock') . '/scripts/dock.js');
	drupal_add_js(drupal_get_path('module', 'dock') . '/scripts/jqDock.min.js');
	drupal_add_css(drupal_get_path('module', 'dock') . '/scripts/dock.css');
}

function dock_menu_tree_output($tree) {
  $output = '';
  $items = array();

  // Pull out just the menu items we are going to render so that we
  // get an accurate count for the first/last classes.
  foreach ($tree as $data) {
    if (!$data['link']['hidden']) {
      $items[] = $data;
    }
  }

  $num_items = count($items);
  foreach ($items as $i => $data) {
    $extra_class = NULL;
    if ($i == 0) {
      $extra_class = 'first';
    }
    if ($i == $num_items - 1) {
      $extra_class = 'last';
    }
		$remember_opts = $data['link']['localized_options'];
    $remember_title = $data['link']['title'];
		$data['link']['localized_options']['html'] = TRUE;
		$data['link']['localized_options']['query'] = '';
		$image = "/images/$remember_title.png";
		$path = drupal_get_path('module', 'dock');
		if (!file_exists(dirname(__FILE__) . $image)) {
			$image = '/images/druplicon.png';
		}
		$data['link']['title'] = '<img src="' . $path . $image . '" alt="' . $remember_title . '" title="' . $remember_title . '" border="0" class="dockItem"/>';
    $link = theme('menu_item_link', $data['link']);
		$data['link']['localized_options'] = $remember_opts;
    $data['link']['title'] = $remember_title;
		if ($data['below']) {
      $output .= theme('menu_item', $link, $data['link']['has_children'], menu_tree_output($data['below']), $data['link']['in_active_trail'], $extra_class);
    }
    else {
      $output .= theme('menu_item', $link, $data['link']['has_children'], '', $data['link']['in_active_trail'], $extra_class);
    }
  }
  return $output ? '<ul id="dock">' . $output . '</ul>' : '';
}

function dock_load_content() {
	$_GET['destination'] = '';
	$dock_menu = variable_get('dock_menu', 0);
	// @todo Fetch $dock_menu data.
	exit('<div class="dock-first-level"><div class="dock-second-level">' . dock_menu_tree_output(menu_tree_page_data('navigation')) . '</div></div>');
}

function dock_admin_settings() {
	$form = array();

	$form['dock_menu'] = array(
		'#default_value'	=>  variable_get('dock_menu', 'primary-links'),
		'#description'		=>	t('Choose the dock menu. Feature currently unavailable.'),
		'#options'				=>	menu_parent_options(menu_get_menus(), array()),
		'#title'					=>	t('Dock menu'),
		'#type'						=>	'select',
		'#disabled'				=>  TRUE,
	);

	$form['dock_position'] = array(
		'#default_value'	=>  variable_get('dock_position', 'bottom'),
		'#description'		=>	t('Choose the dock location.'),
		'#options'				=>	array('bottom' => 'bottom', 'top' => 'top', 'left' => 'left', 'right' => 'right'),
		'#title'					=>	t('Dock location'),
		'#type'						=>	'select',
	);

	$form['dock_size'] = array(
		'#default_value'	=>  variable_get('dock_size', 48),
		'#description'		=>	t('Choose the default(minimum) dock icon size.'),
		'#title'					=>	t('Dock icon size'),
		'#type'						=>	'textfield',
		'#size'						=>  2,
	);

	$form['dock_labels'] = array(
		'#default_value'	=>  variable_get('dock_labels', FALSE),
		'#description'		=>	t('Show menu item titles as icon labels.'),
		'#title'					=>	t('Display labels'),
		'#type'						=>	'checkbox',
	);
	return system_settings_form($form);
}